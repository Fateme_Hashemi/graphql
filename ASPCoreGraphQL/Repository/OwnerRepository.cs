using System.Collections.Generic;
using System.Linq;
using ASPCoreGraphQL.Contracts;
using ASPCoreGraphQL.Models;
using ASPCoreGraphQL.Models.Context;

namespace ASPCoreGraphQL.Repository
{
    public class OwnerRepository : IOwnerRepository
    {
        private readonly ApplicationContext _context;

        public OwnerRepository(ApplicationContext context)
        {
            _context = context;
        }

        public IEnumerable<Owner> GetAll() => _context.Owners.ToList();
    }
}