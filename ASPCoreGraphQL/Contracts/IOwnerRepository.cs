using System.Collections.Generic;
using ASPCoreGraphQL.Models;

namespace ASPCoreGraphQL.Contracts
{
    public interface IOwnerRepository
    {
        IEnumerable<Owner> GetAll();
    }
}