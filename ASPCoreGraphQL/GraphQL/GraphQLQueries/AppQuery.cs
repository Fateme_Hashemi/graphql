using ASPCoreGraphQL.Contracts;
using ASPCoreGraphQL.GraphQL.GraphQLTypes;
using GraphQL.Types;

namespace ASPCoreGraphQL.GraphQL.GraphQLQueries
{
    public class AppQuery : ObjectGraphType
    {
        public AppQuery(IOwnerRepository repository)
        {
            Field<ListGraphType<OwnerType>>(
               "owners",
               resolve: context => repository.GetAll()
           );
        }
    }
}