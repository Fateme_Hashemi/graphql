using ASPCoreGraphQL.GraphQL.GraphQLQueries;
using GraphQL;
using GraphQL.Types;

namespace ASPCoreGraphQL.GraphQL.GraphQLSchema
{
    public class AppSchema : Schema
    {
        public AppSchema(IDependencyResolver resolver)
             : base(resolver)
        {
            Query = resolver.Resolve<AppQuery>();
        }
    }
}