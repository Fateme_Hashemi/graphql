namespace MyProject.IntegrationTest.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using MyProject.IntegrationTest.Constants;
    using MyProject.IntegrationTest.Fixtures;
    using MyProject.IntegrationTest.Models;
    using Xunit;
    using Xunit.Abstractions;

    public class QueryTest : CustomWebApplicationFactory<Startup>
    {
        private readonly HttpClient client;

        public QueryTest(ITestOutputHelper testOutputHelper)
            : base(testOutputHelper) =>
            this.client = this.CreateClient();

        [Fact]
        public async Task IntrospectionQuery_Default_Returns200Ok()
        {
            var response = await this.client.PostGraphQLAsync(GraphQlQuery.Introspection);

            var graphQlResponse = await response.Content.ReadAsAsync<GraphQLResponse>();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Null(graphQlResponse.Errors);
        }
    }
}
