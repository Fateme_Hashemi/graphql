namespace MyProject.IntegrationTest.Models
{
    using System.Collections.Generic;

    public class GraphQLResponse
    {
        public List<GraphQLError> Errors { get; set; }
    }
}
